package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.travelyn.travelyn.Data.NewContactModule;
import com.travelyn.travelyn.Data.UserModule;

/**
 * Created by rendy on 12/11/2017.
 */

public class SignUp extends AppCompatActivity {
    private TextView txtNama;
    private TextView txtEmail;
    private TextView txtPass;
    private TextView txtReType;
    private TextView txtUsername;
    private Button btnSignUp;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        firebaseAuth = FirebaseAuth.getInstance();
        txtNama = (TextView)findViewById(R.id.txtNama);
        txtEmail = (TextView)findViewById(R.id.txtAddFriendEmail);
        txtPass = (TextView)findViewById(R.id.txtPass);
        txtReType = (TextView)findViewById(R.id.txtReType);
        txtUsername = findViewById(R.id.txtUsername);
        btnSignUp = (Button)findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });
    }

    public void signUp(){
        final String nama = txtNama.getText().toString().trim();
        final String email = txtEmail.getText().toString().trim();
        String pass = txtPass.getText().toString().trim();
        final String username = txtUsername.getText().toString().trim();
        String retype = txtReType.getText().toString().trim();

        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(pass)||TextUtils.isEmpty(nama) || TextUtils.isEmpty(retype) || TextUtils.isEmpty(username)){
            Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!TextUtils.equals(pass,retype)){
            Toast.makeText(this, "Password and Retype Password not match", Toast.LENGTH_SHORT).show();
            return;
        }

        firebaseAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    finish();
                    Toast.makeText(SignUp.this, "Register Successful", Toast.LENGTH_SHORT).show();
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    UserProfileChangeRequest setUserNames = new UserProfileChangeRequest.Builder().setDisplayName(nama).build();
                    user.updateProfile(setUserNames);

                    setNewUser(username,nama,email);

                    toMain();
                }
                else{
                    Toast.makeText(SignUp.this, "Register Unsuccessful", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void toMain(){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void setNewUser(String userId, String name, String email){
        UserModule user = new UserModule(name,email);
        NewContactModule ncm = new NewContactModule("newContact",false);
        mDatabase = FirebaseDatabase.getInstance().getReference();


        mDatabase.child("users").child(userId).setValue(user);

        DatabaseReference newContact = mDatabase.child("users").child(userId).child("contact");
        newContact.setValue(ncm);

    }
}
