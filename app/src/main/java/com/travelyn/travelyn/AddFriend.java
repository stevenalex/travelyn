package com.travelyn.travelyn;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.travelyn.travelyn.Data.UserModule;
import com.travelyn.travelyn.Data.NewContactModule;

import com.google.firebase.auth.FirebaseAuth;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 12/4/2017.
 */

public class AddFriend extends AppCompatActivity {
    private TextView txtName;
    private TextView txtEmail;
    private EditText etEmail;
    private Button btnFind;
    private Button btnAdd;

    private FirebaseAuth firebaseAuth;
    private String friendName;
    private String friendEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_addfriend);

        txtName = findViewById(R.id.txtAddFriendName);
        txtEmail = findViewById(R.id.txtAddFriendEmail);
        etEmail = findViewById(R.id.FieldFindEmail);
        btnFind = findViewById(R.id.btnAddFriendFind);
        btnAdd = findViewById(R.id.btnAddFriendNew);

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findFriend();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriend();
            }
        });


        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() == null) {
            finish();
        }

    }

    private void findFriend(){
        String friendEmail = etEmail.getText().toString().trim();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users");

        ref.orderByChild("email").equalTo(friendEmail).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds :dataSnapshot.getChildren()){

                    UserModule um = ds.getValue(UserModule.class);

                    txtName.setText(um.getName());
                    txtEmail.setText(um.getEmail());

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });

    }

    private void addFriend(){
        String userEmail = "";


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            userEmail = user.getEmail();

        } else {
            //error user not found
        }

        ref.orderByChild("email").equalTo(userEmail).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {

                    String names = txtName.getText().toString().trim();
                    NewContactModule ncm = new NewContactModule(names,true);

                    DatabaseReference newContact = snapshot.getRef().child("contact").push();
                    newContact.setValue(ncm);


//                    if(snapshot.child("contact").getValue() != null){
//                        String names = txtName.getText().toString().trim();
//                        NewContactModule ncm = new NewContactModule(names,true);
//
//                        DatabaseReference newContact = snapshot.getRef().child("contact");
//                        newContact.setValue(ncm);
//
////                        Map<String, Object> contactUpdate = new HashMap<>();
////                        contactUpdate.put("contact",txtName.getText().toString().trim());
////                        contactRef.updateChildren(contactUpdate);
//
//
//                    } else {
//                        snapshot.getRef().child("contact").setValue(txtName.getText().toString().trim());//realCode
//                    }

                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });

//        DatabaseReference contact = database.getReference("users").child()

    }

}
