package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by rendy on 12/11/2017.
 */

public class profile extends AppCompatActivity {
    private TextView txtName;
    private TextView txtEmail;
    private Button btnEdit;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);


        txtName = findViewById(R.id.txtAddFriendName);
        txtEmail = findViewById(R.id.txtAddFriendEmail);
        btnEdit = findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toEdit();
            }
        });

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        final DatabaseReference ref = db.getReference("userData");

//        UserModule um = new UserModule();

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){
            finish();
            toLogin();
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            String name = user.getDisplayName();
            String email = user.getEmail();

            txtName.setText("Name : " + name);
            txtEmail.setText("Email : " + email);

        }

    }

    private void toLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    private void toEdit(){
        Intent intent = new Intent(this, EditUserData.class);
        startActivity(intent);
    }
}
