package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

/**
 * Created by Steven on 12/3/2017.
 */

public class EditUserData extends AppCompatActivity {
    private EditText etName;
    private EditText etEmail;
    private Button btnSave;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_edit);

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.FieldFindEmail);
        btnSave = findViewById(R.id.btnSave);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){
            finish();
            toLogin();
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            etName.setText(user.getDisplayName().toString());
            etEmail.setText(user.getEmail().toString());

        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateData();
            }
        });

    }

    private void toLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    private void toHome(){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

    public void toProfile(){
        Intent intent = new Intent(this, profile.class);
        startActivity(intent);
    }

    private void updateData(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        String name = etName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();


        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(email)){
            Toast.makeText(this, "Name or Email field cannot be empty", Toast.LENGTH_SHORT).show();
        } else {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(etName.getText().toString())
                    .build();



            try {

                user.updateProfile(profileUpdates);
                user.updateEmail(etEmail.getText().toString());
                Toast.makeText(this, "Profile Succesfully Updated", Toast.LENGTH_SHORT).show();
                toHome();
//                toProfile();

            } catch(Exception e){
                Toast.makeText(this, "Failed to Update Profile", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
