package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.travelyn.travelyn.Data.UserModule;
import com.travelyn.travelyn.Data.NewContactModule;

import java.util.ArrayList;

/**
 * Created by Steven on 12/2/2017.
 */

public class community extends AppCompatActivity {
    private Button btnAdd;
    private ListView lvFriends;
    private FirebaseAuth firebaseAuth;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> listNama;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_community);

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() == null) {
            finish();
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users");

        btnAdd = findViewById(R.id.btnAddFriend);
        lvFriends = findViewById(R.id.lvFriends);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriend();
            }
        });


        listNama = new ArrayList<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){

                    UserModule userModule = ds.getValue(UserModule.class);

                    String nama = userModule.getName();

//                    Toast.makeText(community.this, nama, Toast.LENGTH_SHORT).show();
                    listNama.add(nama);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(community.this, "Error : Load Friends", Toast.LENGTH_SHORT).show();
            }
        });

//        DatabaseReference contactRef = database.getReference("users");
//
//        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//        String userEmail = user.getEmail().toString();
//
//        contactRef.orderByChild("email").equalTo(userEmail).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for(DataSnapshot ds : dataSnapshot.child("contact").getChildren()){
//
////                    UserModule userModule = ds.getValue(UserModule.class);
//                    NewContactModule ucm = ds.getValue(NewContactModule.class);
//
////                    String nama = userModule.getName();
//
//                    String nama = ucm.getName();
////                    Toast.makeText(community.this, nama, Toast.LENGTH_SHORT).show();
//                    listNama.add(nama);
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        adapter = new ArrayAdapter<String>(community.this, android.R.layout.simple_list_item_1, listNama);
        lvFriends.setAdapter(adapter);

    }

    private void addFriend() {
        Intent intent = new Intent(this, AddFriend.class);
        startActivity(intent);
    }
}
