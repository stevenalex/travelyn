package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.travelyn.travelyn.Data.PlacesModule;


import java.util.ArrayList;

/**
 * Created by Alex on 11/12/2017.
 */

public class search extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private SearchView svSearch;
    private ListView lvSearch;
    private Button btnSearch;
    private ImageButton ibMap;
    private ArrayList<String> listPlaces = new ArrayList<>();;
    private ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){
            finish();
//            toLogin();
        }

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("places");

        btnSearch = findViewById(R.id.btnSearch);
        ibMap = findViewById(R.id.ibMap);
        svSearch = findViewById(R.id.svSearch);
        lvSearch = findViewById(R.id.lvSearch);
        ibMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toMap();
            }
        });

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    PlacesModule pm = ds.getValue(PlacesModule.class);

                    String name = pm.getName();

//                    Toast.makeText(search.this, name, Toast.LENGTH_SHORT).show();
                    listPlaces.add(name);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(search.this, "Error : Load Search", Toast.LENGTH_SHORT).show();
            }
        });
        adapter = new ArrayAdapter<String>(search.this, android.R.layout.simple_list_item_1, listPlaces);
        lvSearch.setAdapter(adapter);

        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

//        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, searchResult);
//        lvSearch.setAdapter(adapter);

    }
    public void toMap(){
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
}
