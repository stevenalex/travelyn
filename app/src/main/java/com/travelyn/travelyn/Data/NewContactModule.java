package com.travelyn.travelyn.Data;

/**
 * Created by Alex on 12/4/2017.
 */

public class NewContactModule {
    public String name = "";
    public boolean isContact = false;


    public NewContactModule(){

    }

    public NewContactModule(String name, boolean isContact){
        this.setName(name);
        this.setContact(isContact);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isContact() {
        return isContact;
    }

    public void setContact(boolean contact) {
        isContact = contact;
    }
}
