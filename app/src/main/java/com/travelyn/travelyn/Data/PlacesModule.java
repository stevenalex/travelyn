package com.travelyn.travelyn.Data;

/**
 * Created by Alex on 12/4/2017.
 */

public class PlacesModule {
    public String name;
    public String location;
//    public String lat;
//    public String lng;
//    public String detail;

    public PlacesModule(){

    }

    public PlacesModule(String name, String location){
        this.setName(name);
        this.setLocation(location);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
