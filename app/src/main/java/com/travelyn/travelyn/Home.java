package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by rendy on 12/11/2017.
 */

public class Home extends AppCompatActivity {
    private ImageButton ibProfile;
    private ImageButton ibSearch;
    private ImageButton ibCommunity;
    private ImageButton ibLogout;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ibProfile = findViewById(R.id.ibProfile);
        ibSearch = findViewById(R.id.ibSearch);
        ibCommunity = findViewById(R.id.ibCommunity);
        ibLogout = findViewById(R.id.ibLogout);

        ibProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toProfile();
            }
        });
        ibSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toSearch();
            }
        });

        ibCommunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toCommunity();
            }
        });

        ibLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toLogout();
            }
        });


    }

    public void toProfile(){
        Intent intent = new Intent(this, profile.class);
        startActivity(intent);
    }
    public void toSearch(){
        Intent intent = new Intent(this, search.class);
        startActivity(intent);
    }
    public void toCommunity(){
        Intent intent = new Intent(this, community.class);
        startActivity(intent);
    }

    public void toLogout(){
        this.finish();
        firebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        startActivity(new Intent(Home.this, MainActivity.class)); //Go back to home page
        finish();
    }
}
