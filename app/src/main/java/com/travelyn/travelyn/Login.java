package com.travelyn.travelyn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


/**
 * Created by rendy on 12/11/2017.
 */

public class Login extends AppCompatActivity {
    private EditText txtEmail;
    private EditText txtPass;
    private TextView txtSignUp;
    private Button btnLogin;
    private FirebaseAuth firebaseAuth;
    private CallbackManager callbackManager;
    private LoginButton loginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        setContentView(R.layout.login);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser()!=null){
            finish();
            toMain();
        }
        if(Profile.getCurrentProfile()!=null){
            finish();
            toMain();
        }

        //facebook
        LoginButton loginButton = findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                finish();
                Toast.makeText(Login.this, "Login Success",Toast.LENGTH_SHORT).show();
                toMain();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(Login.this, "Login Failed",Toast.LENGTH_SHORT).show();
            }
        });

        txtEmail = findViewById(R.id.txtAddFriendEmail);
        txtPass = findViewById(R.id.txtPass);
        txtSignUp = findViewById(R.id.txtSignUp);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Login.this, "SignUp Tapped",Toast.LENGTH_SHORT).show();
                toSignUp();
            }
        });
    }

    public void login(){
        String email = txtEmail.getText().toString().trim();
        String pass = txtPass.getText().toString().trim();

        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(pass)){
            Toast.makeText(Login.this, "Empty Email & Password", Toast.LENGTH_SHORT).show();
            return;
        }
        firebaseAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    finish();
                    Toast.makeText(Login.this, "Login Success",Toast.LENGTH_SHORT).show();
                    toMain();
                }
                else{
                    Toast.makeText(Login.this, "Login Failed",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void toSignUp(){
        Intent intent = new Intent(this, SignUp.class);
        startActivity(intent);
    }

    public void toMain(){
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }
    public void toProfile(){
        Intent intent = new Intent(this, profile.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
